# HelloAngular

## example page
* [Sample Page](http://34.211.24.236/)
* [angular 관련 내용](https://kjkjjang.wordpress.com/category/개발/angular/)

## project settings
* `apt-get install` // install node  
* // install npm 
* `npm install -g http-server` // using http-server 
* `npm install -g typescript` // using typescript 
* `npm install -g @angular/cli` // using angular cli 

## start new project
* `ng new hello-angular` // new angular cli project 
* `ng serve` // start service (if invalid host header shows then `ng serve --port [port] --publicHost [ip]`)

## component generate
* `ng generate component [name]` // angular cli generate new component -> src/app/[component] created
* `ng generate service [name]` // angular cli generate new service -> src/app/component/[service] created
* `ng generate pipe [name]` // angular cli generate new pipe -> src/app/component/[pipe] created
* `ng generate enum [name]` // angular cli generate new enum -> src/app/component/[enum] created

## use module
* `npm install --save @angular/animations @angular/material hammerjs`

