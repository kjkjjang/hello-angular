import { InjectionToken } from '@angular/core';
import { MyServiceLoggerService } from './mouse-point-logger/my-service-logger.service';
import { LogLevel } from './mouse-point-logger/log-level.enum';
export const LOG_LEVEL_TOKEN = new InjectionToken<LogLevel>('logLevel');
