import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'langSelectorBtn' // 현재 pipe 모듈의 명칭
})
export class LangSelectorBtnPipe implements PipeTransform {

  transform(lang): any {
    return `${lang.name} (${lang.code})`;
  }

}
