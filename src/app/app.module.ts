import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';


import { WelcomeMsgComponent } from './welcome-msg/welcome-msg.component';
import { LangSelectorComponent } from './lang-selector/lang-selector.component';

import { FormsModule, COMPOSITION_BUFFER_MODE} from '@angular/forms';

import { I18nSupportService } from './welcome-msg/i18n-support.service';
import { LangSelectorBtnPipe } from './lang-selector/lang-selector-btn.pipe';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MdToolbarModule, MdSnackBarModule, MdCardModule, MdInputModule, MdRadioModule, MdButtonModule } from '@angular/material';
import { NumberCounterComponent } from './number-counter/number-counter.component';
import { NgDirectivesComponent } from './ng-directives/ng-directives.component';
import { MousePointLoggerComponent } from './mouse-point-logger/mouse-point-logger.component'

import { MyServiceLoggerService } from './mouse-point-logger/my-service-logger.service';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeMsgComponent,
    LangSelectorComponent,
    LangSelectorBtnPipe,
    NumberCounterComponent,
    NgDirectivesComponent,
    MousePointLoggerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdToolbarModule, MdSnackBarModule, MdCardModule, MdInputModule, MdRadioModule, MdButtonModule
  ],
  providers: [
    {provide: COMPOSITION_BUFFER_MODE, useValue: false},
    I18nSupportService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
