import { Injectable } from '@angular/core';
import { LANG_METADATA } from '../lang-selector/lang-metadata';

@Injectable()
export class I18nSupportService {
  langCode = 'ko';
  private welcomeMsg: { [key: string]: string } ;

  constructor() {
   }

   getWelcomeMsg(userName: string) {
     const langObj = LANG_METADATA.find(lang => lang.code === this.langCode);
     return `${langObj.msg}, ${userName}!`;
   }
}
