import { Component, AfterViewInit } from '@angular/core';
import { I18nSupportService } from './i18n-support.service';
import { MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-welcome-msg',
  templateUrl: './welcome-msg.component.html',
  styleUrls: ['./welcome-msg.component.css']
})
export class WelcomeMsgComponent implements AfterViewInit {
  static CHK_KEYUP_WAIT_SEC;
  userName: string;
  welcomeMsg: string;

  constructor(public i18nSupporter: I18nSupportService, private snackbar: MdSnackBar) {
    WelcomeMsgComponent.CHK_KEYUP_WAIT_SEC = 3000;
  }

  ngAfterViewInit(): void {
    const checkTouchedFn = () => {
      if (this.userName != null && this.userName.length > 0) { return };
      this.snackbar.open('이름을 입력해 주세요', '확인', {duration: 3000});
    }
    setTimeout(checkTouchedFn, WelcomeMsgComponent.CHK_KEYUP_WAIT_SEC)
  }

  setName(name) {
    this.userName = name;
  }

  showWelcomeMsg(code) {
    this.welcomeMsg = this.i18nSupporter.getWelcomeMsg(this.userName);
  }

}
