import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberCounterComponent } from './number-counter.component';

import { COMPOSITION_BUFFER_MODE} from '@angular/forms';
import { MdSnackBar } from '@angular/material';
import { MdToolbarModule, MdSnackBarModule, MdCardModule, MdInputModule, MdRadioModule, MdButtonModule } from '@angular/material';


describe('NumberCounterComponent', () => {
  let component: NumberCounterComponent;
  let fixture: ComponentFixture<NumberCounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumberCounterComponent ],
      imports: [
        MdToolbarModule, MdSnackBarModule, MdCardModule, MdInputModule, MdRadioModule, MdButtonModule
      ],
      providers: [    {provide: COMPOSITION_BUFFER_MODE, useValue: false} 
      ]
      
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
