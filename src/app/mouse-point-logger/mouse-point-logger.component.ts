import { Component, OnInit } from '@angular/core';
import { MyServiceLoggerService } from './my-service-logger.service';
import { LogLevel } from './log-level.enum';
import { LOG_LEVEL_TOKEN } from '../app.tokens';

@Component({
  selector: 'app-mouse-point-logger',
  templateUrl: './mouse-point-logger.component.html',
  styleUrls: ['./mouse-point-logger.component.css'],
  providers: [MyServiceLoggerService, {provide: LOG_LEVEL_TOKEN, useValue: LogLevel.DEBUG}]
})
export class MousePointLoggerComponent {

  logMessage: string;

  // logger component injection
  constructor(public logService: MyServiceLoggerService) {
//    this.logLevel = LogLevel.INFO;
//    this.logService = new MyServiceLoggerService(this.logLevel);
//    this.init();
  }

  captureMousePos($event: MouseEvent) {
    const position = [$event.clientX, $event.clientY];
    const positionMessage = `x[${position[0]}] y[${position[1]}]`;
    this.logService.log(positionMessage);
    this.logMessage = positionMessage;
  }

  init() {
    console.log('=========== Info Log Level ===========');
    this.logService.debug('test logging debug');
    this.logService.info('test logging info');
    this.logService.warn('test logging warn');
    this.logService.error('test logging error');

    this.logService.logLevel = LogLevel.DEBUG;
    console.log('=========== Debug Log Level ===========');
    this.logService.debug('test logging debug');
    this.logService.info('test logging info');
    this.logService.warn('test logging warn');
    this.logService.error('test logging error');

    this.logService.logLevel = LogLevel.WARN;
    console.log('=========== WARN Log Level ===========');
    this.logService.debug('test logging debug');
    this.logService.info('test logging info');
    this.logService.warn('test logging warn');
    this.logService.error('test logging error');

    this.logService.logLevel = LogLevel.ERROR;
    console.log('=========== ERROR Log Level ===========');
    this.logService.debug('test logging debug');
    this.logService.info('test logging info');
    this.logService.warn('test logging warn');
    this.logService.error('test logging error');
  }

}
