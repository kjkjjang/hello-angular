import { Injectable, Inject } from '@angular/core';
import { LogLevel } from './log-level.enum';
import * as format from 'date-fns/format';
import * as token from './../app.tokens'

@Injectable()
export class MyServiceLoggerService {

  public logLevel: LogLevel;
  public logs: string[];
  private readonly MAX_HISTORY_COUNT: number;
  private readonly TIME_FORMATTER: string;

  constructor(@Inject(token.LOG_LEVEL_TOKEN) logLevel: LogLevel) {
    this.logLevel = logLevel;
    this.logs = [];
    this.MAX_HISTORY_COUNT = 100;
    this.TIME_FORMATTER = 'YYYY-MM-DD HH:mm:ss.SSS';
  }

  private getFormattedLogMsg(logLevel: LogLevel, msg: string) {
    const currentTimestamp = format(new Date(), this.TIME_FORMATTER);
    return `[${LogLevel[logLevel]}] ${currentTimestamp} - ${msg}`;
  }

  private isProperLogLevel(logLevel: LogLevel): boolean {
    if (this.logLevel === LogLevel.DEBUG) {
      return true;
    } else {
      return logLevel >= this.logLevel;
    }
  }

  private keepLogHistory(log: string) {
    if (this.logs.length === this.MAX_HISTORY_COUNT) {
      this.logs.shift();
    }
    this.logs.push(log);
  }

  private logging(logLevel: LogLevel, msg: string) {
    const logMsg = this.getFormattedLogMsg(logLevel, msg);
    if (this.isProperLogLevel(logLevel)) {
      console.log(logMsg);
      this.keepLogHistory(logMsg);
    }
  }

  log(msg: string) {
    this.logging(this.logLevel, msg);
  }

  public debug(msg: string) { this.logging(LogLevel.DEBUG, msg); }
  public info(msg: string) { this.logging(LogLevel.INFO, msg); }
  public warn(msg: string) { this.logging(LogLevel.WARN, msg); }
  public error(msg: string) { this.logging(LogLevel.ERROR, msg); }

}
