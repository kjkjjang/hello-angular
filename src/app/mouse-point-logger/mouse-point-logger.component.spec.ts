import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MousePointLoggerComponent } from './mouse-point-logger.component';
import { By } from '@angular/platform-browser';
import { MyServiceLoggerService } from './my-service-logger.service';
import { LogLevel } from './log-level.enum';
import { LOG_LEVEL_TOKEN } from './../app.tokens';
import { COMPOSITION_BUFFER_MODE} from '@angular/forms';
import { MdSnackBar } from '@angular/material';
import { MdToolbarModule, MdSnackBarModule, MdCardModule, MdInputModule, MdRadioModule, MdButtonModule } from '@angular/material';

describe('MousePointLoggerComponent', () => {
  let component: MousePointLoggerComponent;
  let fixture: ComponentFixture<MousePointLoggerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MousePointLoggerComponent ],
      imports: [
        MdToolbarModule, MdSnackBarModule, MdCardModule, MdInputModule, MdRadioModule, MdButtonModule
      ],
      providers: [    {provide: COMPOSITION_BUFFER_MODE, useValue: false},
        MyServiceLoggerService, {provide: LOG_LEVEL_TOKEN, useValue: LogLevel.INFO}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MousePointLoggerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
  
  it('mouse 클릭 시 로그 적재 테스트', () => {
    expect(component).toBeTruthy();
    
    const trackZone = fixture.debugElement.query(By.css('.track-zone'));
    trackZone.triggerEventHandler("click", {clientX: 1, clientY: 1});
    expect( (<MyServiceLoggerService>component.logService).logs.length).toEqual(1);
    
    trackZone.triggerEventHandler("click", {clientX: 10, clientY: 10});
    expect( (<MyServiceLoggerService>component.logService).logs.length).toEqual(2);
  });
  
});
