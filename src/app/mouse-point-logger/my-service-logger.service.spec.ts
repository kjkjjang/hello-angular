import { TestBed, inject } from '@angular/core/testing';

import { MyServiceLoggerService } from './my-service-logger.service';

import { LogLevel } from './log-level.enum';
import { LOG_LEVEL_TOKEN } from './../app.tokens';

describe('MyServiceLoggerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyServiceLoggerService, {provide: LOG_LEVEL_TOKEN, useValue: LogLevel.INFO}]
    });
  });

  it('should be created', inject([MyServiceLoggerService], (service: MyServiceLoggerService) => {
    expect(service.logLevel).toEqual(LogLevel.INFO);
  }));
  
  it('LogLevel 변경 검사', inject([MyServiceLoggerService], (service: MyServiceLoggerService) => {
    service.logLevel = LogLevel.DEBUG;
    expect(service.logLevel).toEqual(LogLevel.DEBUG);
  }));
  
  it('log size 검사', inject([MyServiceLoggerService], (service: MyServiceLoggerService) => {
    service.info("test 1");
    service.info("test 2");
    expect(service.logs.length).toEqual(2);
    service.debug("is not printed");
    expect(service.logs.length).toEqual(2);
    service.logLevel = LogLevel.ERROR;
    service.debug("is not printed");
    service.info("is not printed");
    service.warn("is not printed");
    service.error("test 3");
    expect(service.logs.length).toEqual(3);
  }));

});
